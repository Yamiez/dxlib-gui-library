#pragma once
#include "lib.h"
#include <vector>
#include <thread>

begin_conc 

namespace details {


}


class thread_pool
{
public:
	thread_pool( );
	~thread_pool( );


	template<typename..._Args>
	std::thread &start( _Args&&...args ) { threads_.emplace_back( std::forward<_Args>( args )... ); return threads_.back( ); }

	void wait_all( );

private:
	std::vector<std::thread> threads_;
};


end_conc