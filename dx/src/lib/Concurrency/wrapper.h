#pragma once
#include "lib.h"
#include <mutex>

begin_conc

template<typename _Ty, typename _Mtx>
class mutex_wrapper
{
private:
	std::lock_guard<_Mtx> guard_;
	_Ty *ty_;

public:
	mutex_wrapper( _Mtx &mtx, _Ty *ty )
		: guard_( mtx ), ty_( ty )
	{}

	mutex_wrapper( mutex_wrapper &&other )
		: guard_( std::move( other.guard_ ) ), ty_( std::move( other.ty_ ) )
	{}


	_Ty *operator->( ) { return ty_; }
};


end_conc