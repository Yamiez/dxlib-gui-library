#include "thread_pool.h"

begin_conc

thread_pool::thread_pool()
	: threads_( )
{
}

thread_pool::~thread_pool()
{
	for ( auto &x : threads_ )
		x.detach( );
}

void thread_pool::wait_all()
{
	for ( auto &x : threads_ )
		if ( x.joinable( ) )
			x.join( );
}


end_conc

