#pragma once
#include "wrapper.h"


begin_conc


template<typename _Ty, typename _Mtx = std::mutex>
class concurrent_object
{
private:
	_Mtx mtx_;
	_Ty *ty_;

public:
	concurrent_object( _Ty *ptr )
		: ty_( ptr ), mtx_( )
	{}

	concurrent_object( )
		: ty_( nullptr ), mtx_( )
	{}

	_Ty *setPtr( _Ty *ptr )
	{
		auto tmp = ty_;
		ty_ = ptr;
		return tmp;
	}

	mutex_wrapper<_Ty, _Mtx> operator->( ) { return mutex_wrapper<_Ty, _Mtx>( mtx_, ty_ ); }

	_Mtx &aquire_lock( ) { return mtx_; }

	_Ty *aquire_ptr_no_lock( ) { return mtx_; }
	
};


end_conc

