#include "src/api"


struct Vector3
{
	float x, y, z;


	template<typename X, typename Y, typename Z>
	Vector3( X &&x, Y &&y, Z &&z )
		: x( std::forward<X>( x ) ), y( std::forward<Y>( y ) ), z( std::forward<Z>( z ) )
	{}

	Vector3( )
		: Vector3( 0.0f, 0.0f, 0.0f )
	{}

	
};

struct bezier_util
{
	static float bezier_point( float pa, float pb, float perc ) { auto diff = pa - pb; return pa + ( diff * perc ); }
};


void MouseClicked( dx::Window *sender, dx::MouseClickedArgs &args );
void Paint( dx::Window *sender, dx::BasePainter *painter );

int main( )
{
	auto appl = dx::Application::Create( );
	auto window = dx::Window::Create( "bezier", "bezier test", { { 300, 300 }, { 600, 400 } } );
	window->Show( );
	window->BringToTop( );
	window->SpecializePaint( dx::Window::OnTick_t );

	auto painter = dx::Painter::Create( window );

	// Events...
	window->OnMouseClicked( ) += MouseClicked;
	window->OnPaint( ) += Paint;
	window->OnWindowClosed( ) += []( dx::Window *sender ) { dx::Application::get( )->exit( );  };

	
	return appl->run( );
}

// other
dx::Vector2 point_one, point_two, point_three;
dx::DeltaTimer timer;
float paint_time;
dx::uint count_click;

void MouseClicked(dx::Window * sender, dx::MouseClickedArgs & args)
{
	if ( count_click == 0 )
		point_one = args.position;
	else if ( count_click == 1 )
		point_two = args.position;
	else
	{
		point_three = args.position;
		count_click = 0;
		return;
	}

	++count_click;
}

void Paint(dx::Window * sender, dx::BasePainter * painter)
{
	painter->PaintRect( { { 0, 0 }, { sender->Width( ), sender->Height( ) } }, dx::Pen( 0xff2d2d2d ) );
	paint_time += timer.GetDeltaTime( );


	if ( point_one.x == 0.0f && point_two.x == 0.0f && point_three.x == 0.0f &&
		point_one.y == 0.0f && point_two.y == 0.0f && point_three.y == 0.0f )
		return; 

	// Paint 1 -> 2 -> 3 triangle ^
	{
		painter->PaintRect( { { point_one.x, point_one.y }, { 6, 6 } }, dx::Pen( dx::Colors::Gray, 1 ) );
		painter->PaintRect( { { point_two.x, point_two.y }, { 6, 6 } }, dx::Pen( dx::Colors::Gray, 1 ) );
		painter->PaintRect( { { point_three.x, point_three.y }, { 6, 6 } }, dx::Pen( dx::Colors::Gray, 1 ) );
	}

	if ( paint_time >= 1.5f )
		paint_time = 0.0f;

	if ( paint_time >= 1.0f )
		return;

	
	// Paint bezier progress
	dx::Vector2 last{ 0, 0 };
	for ( auto i = 0.0f; i < paint_time; i += 0.01f )
	{
		auto xa = bezier_util::bezier_point( point_one.x, point_two.x, i );
		auto ya = bezier_util::bezier_point( point_one.y, point_two.y, i );
		auto xb = bezier_util::bezier_point( point_two.x, point_three.x, i );
		auto yb = bezier_util::bezier_point( point_two.y, point_three.y, i );

		auto x = bezier_util::bezier_point( xa, xb, i );
		auto y = bezier_util::bezier_point( ya, yb, i );

		if ( last.x == 0.0f || last.y == 0.0f )
			painter->PaintRect( { { x, y }, { 2, 2 } }, dx::Pen( dx::Colors::Black, 1 ) );
		else
			painter->PaintLine( dx::Line( last, { x, y }, dx::Pen( dx::Colors::Black, 2 ) ) );
		last = { x, y };
	}
}
